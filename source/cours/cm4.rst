================================
 Pages dynamiques en Javascript
================================

.. role:: lat
.. role:: en

.. highlight:: javascript

Motivation
==========

Architecture Client-Serveur
---------------------------

.. figure:: client-server.png
   :height: 8ex

   Source image http://commons.wikimedia.org/wiki/File:Client-server-model.svg

* Jusqu'à maintenant, le gros du travail était fait par le serveur.

* On souhaite pouvoir déporter une partie de la **logique applicative**
  coté client.

Programmation coté client
-------------------------

* Ceci suppose d'avoir un langage de programmation *généraliste*
  (≠ HTML/CSS) compris par tous les navigateurs.

* Actuellement, ce langage est Javascript.

.. note::

  Javascript a beaucoup évolué au cours de son histoire.

  La version que nous présentons ici est ES6,
  une version relativement récente (2015)
  qui a apporté beaucoup de nouveautés au langage,
  mais également des incompatibilités avec les versions précédentes.

  Gardez cela en tête lorsque vous trouverez des exemples
   en ligne.



Syntaxe
=======

Inspiration
-----------

* Comme son nom l'indique,
  la syntaxe de Javascript est (librement) inspirirée de celle de Java
  (ou du C).

* La similitude s'arrête là :
  Javascript n'est pas basé sur Java.


Condition
---------

.. container:: comparison

  .. code-block:: javascript

     if (i < 10) {
         j = j+1;
         k += i;
     } else {
         j = 0;
     }

  .. code-block:: python

     if i < 10:
         j = j+1
         k += i
     else:
         j = 0



Boucles
-------

.. container:: comparison

  .. code-block:: javascript

     while (i < 10) {
         j = j*i;
         i += 1;
     }

  .. code-block:: python

     while i < 10:
         j = j*i
         i += 1


.. container:: comparison

  .. code-block:: javascript

     for(let i of [1,1,2,3,5,8]) {
         j = j*i;
     }

  .. code-block:: python

     for i in [1,1,2,3,5,8]:
         j = j*i

.. container:: comparison

  .. code-block:: javascript

     for(let i=2; i<1000; i=i*i) {
         console.log(i);
     }

  .. code-block:: python

     i = 2
     while i<1000:
         print(i)
         i = i*i


Fonctions
---------

.. container:: comparison

  .. code-block:: javascript

    function fact(n) {
      let f = 1;
      while (n>1) {
        f = f*n;
        n -= 1;
      }
      return f;
    }

  .. code-block:: python

    def fact(n):
      f = 1
      while n>1:
        f = f*n
        n -= 1
      return f

Exceptions
----------

.. container:: comparison

  .. code-block:: javascript

     if (i < 0) {
         throw new Error(
           "negative value");
     }

  .. code-block:: python

     if i < 0:
         raise Exception(
           "negative value")

.. container:: comparison

  .. code-block:: javascript

     try {
         i = riskyFunction();
     }
     catch (err) {
         i = -1;
     }

  .. code-block:: python

     try:
         i = riskyFunction()
     except Exception as err:
         i = -1

Tableaux
--------
.. container:: comparison

  .. code-block:: javascript

     let a = [4,1,3,6,4];
     let i = 1;
     while (i<a.length) {
       a[i] = a[i]+a[i-1];
       i += 1;
     }

  .. code-block:: python

     a = [4,1,3,6,4]
     i = 1
     while i < len(a):
       a[i] = a[i]+a[i-1]
       i += 1

.. warning::

   Contrairement à Python, un tableau vide (de longueur 0)
   est équivalent à ``true`` dans une condition.

Objets / Dictionnaires
----------------------

.. note::

  En Javascript,
  ce qu'on appelle "objet" se rapproche plus des dictionnaires de Python.

.. container:: comparison

  .. code-block:: javascript

     let p = {
       "nom": "Doe",
       prénom: "John",
     };
     console.log(p["prénom"])
     console.log(p.prénom)

  .. code-block:: python

     p = {
       "nom": "Doe",
       "prénom": "John",
     }
     print(p["prénom"])

Pièges
------

* Javascript est un langage souvent décrié.

  .. figure:: js_books.*

     Photo by `Eliot Ball`_

     .. _Eliot Ball: https://softwareengineering.stackexchange.com/users/62355/eliot-ball


* De fait, il comporte de nombreux pièges.

* Une bonne manière de les éviter est d'utiliser systématiquement
  un `linter`:en: tel que JSHint_ (intégré à de nombreux éditeurs).

.. _JSHint: http://jshint.com/


Déclaration des variables locales
---------------------------------

Contrairement à Python (et à d'autres langages dynamiquement typés),
Javascript demande que les variables locales soient déclarées,
avec le mot-clé ``let``.

.. hint::

   Les paramètres des fonctions font exception à cette règle,
   puisqu'ils sont déclarés par leur présence dans l'en-tête de la fonction.

.. code::

  function fact(n) {
    let f = 1;
    for (let i=2; i<=n; i+=1) {
      f = f*i;
    }
    return f;
  }

.. nextslide::

.. warning::

   Un oubli du mot-clé ``let`` est dangereux, car

   * il ne constitue pas une erreur,
   * mais dans ce cas la variable est considérée comme globale,
     ce qui peut créer des bugs difficiles à détecter,
     comme le montre
     `cet exemple`__.

   __ http://pythontutor.com/javascript.html#code=//%20exemple%20des%20m%C3%A9faits%20des%20variables%20globales%0Avar%20i%3B%20//%20n%C3%A9cessaire%20pour%20pythontutor,%20qui%20refuse%20les%20variables%20globales%20implicites%0A%0Afunction%20fact%28n%29%20%7B%0A%20%20var%20r%20%3D%201%3B%0A%20%20//%20on%20oublie%20le%20'var'%20devant%20i,%20qui%20devient%20donc%20une%20variable%20globale%0A%20%20for%20%28i%3D1%3B%20i%3C%3Dn%3B%20i%2B%3D1%29%20%7B%0A%20%20%20%20r%20%3D%20r*i%3B%0A%20%20%7D%0A%20%20return%20r%3B%0A%7D%0A%0Afunction%20affiche_factorielles%28%29%20%7B%0A%20%20//%20on%20oublie%20le%20'var'%20devant%20i,%20qui%20devient%20donc%20une%20variable%20globale%0A%20%20//%20%28la%20m%C3%AAme%20que%20dans%20fact%29%0A%20%20i%20%3D%201%3B%0A%20%20while%20%28i%3C10%29%20%7B%0A%20%20%20%20console.log%28%22fact%28%22%2Bi%2B%22%29%3D%22%2Bfact%28i%29%29%3B%0A%20%20%20%20i%20%3D%20i%2B1%3B%0A%20%20%7D%0A%7D%0A%0Aaffiche_factorielles%28%29%3B%0A%0A//%20on%20s'attendrait%20%C3%A0%20ce%20que%20cela%20affiche%20toutes%20les%20factorielles%20de%201%20%C3%A0%209,%0A//%20et%20%C3%A7a%20n'affiche%20que%20les%20factorielles%20impaires&curInstr=0&mode=display&origin=opt-frontend.js&py=js&rawInputLstJSON=%5B%5D

.. note::

   JSHint_ détecte ce type d'erreur dans la plupart des cas,
   à condition d'activer le contrôle ainsi ::

     // jshint undef:true

Tests d'égalité
---------------

En JS, on teste l'égalité avec l'opérateur ``===``,
et l'inégalité avec l'opérateur ``!==``\ ::

    if (i === j  &&  i !== k) // ...

.. warning::

   Les opérateurs habituels ``==`` et ``!=`` existent aussi,
   mais ils ont une sémantique très inhabituelle,
   et sont donc généralement évités.

.. note::

   En fait, l'opérateur ``==`` considère que
   deux valeurs de types différents sont égales
   si on peut passer de l'une à l'autre par conversion de type.

   Par exemple ::

     "42" == 42 // est vrai

   C'est un problème, car cela conduit à des choses contre-intuitives ::

     if (a == b  &&  b == c) {
         a == c; // peut être faux
         // par exemple: a = "0", b = 0, c = "" ;
         // en effet, 0 et "" sont tous deux équivalents à false
     }

   ou encore ::

     if (a == b) {
         a+1 == b+1; // peut être faux
         // par exemple: a = "42" (a+1 == "421") et b = 42 (b+1 == 43)
     }

   JSHint signale toute utilisation de ``==`` ou ``!=``.

   Si l'option n'est pas activée par défaut ::

     // jshint:: eqeqeq:true

``null`` et ``undefined``
-------------------------

* En Python, il n'existe qu'une seule valeur « nulle » (``None``).

* En JS, il en existe deux : ``null`` et ``undefined``.

* Elles sont utilisées dans des contextes un peu différent ;
  pour l'instant, retenez surtout que les deux existent.

.. hint::

   Les deux sont équivalentes à ``false`` dans une condition.


Éléments inexistants
--------------------

En Javascript,
l'accès à un élément inexistant dans un tableau ou un objet
ne déclenche pas d'erreur,
mais retourne simplement ``undefined``.

Ceci cause en général une erreur *ailleurs* dans le code,
ce qui rend plus difficile le débogage ::

  let p = { "prénom": "John" };
  let n = p.nom;  // n reçoit undefined, pas d'erreur
  if (n.length > 32) { // erreur: undefined n'a pas de longueur
    // ...
  }





.. _dom:

L'arbre DOM
===========

Présentation
------------

La structure d'un fichier HTML peut être vue comme un *arbre*.

.. graphviz::

   graph  {

     node [ shape=box, style=rounded ]

     html -- head
      head -- title -- title_txt
     html -- body
      body -- h1 -- h1_txt
      body -- p
       p -- p_txt
       p -- a -- a_txt
      body -- img

     a         [ label="a\nhref='./link'" ]
     img       [ label="img\nsrc='./pic'" ]
     title_txt [ shape=box, style=filled, label="Le titre" ]
     h1_txt    [ shape=box, style=filled, label="Le titre" ]
     p_txt     [ shape=box, style=filled, label="Bonjour le " ]
     a_txt     [ shape=box, style=filled, label="monde" ]

   }

Terminologie
------------

* Cet arbre s'appelle l'arbre DOM (pour :en:`Document Object Model`).

* Les nœuds correspondant aux balises sont appelés des **éléments**.

* Les nœuds contenant le contenu textuels sont simplement appelés des « nœuds texte ».

.. note::

   Il existe d'autres types de nœuds (par exemple les nœuds commentaire),
   mais ils sont plus rarement utiles.

L'objet ``document``
--------------------

En JS, la variable globale ``document``
contient un objet représentant le document HTML.
Elle permet d'accéder aux éléments du document :

* `document.getElementById <http://devdocs.io/dom/document/getelementbyid>`_,
* `document.getElementsByTagName <http://devdocs.io/dom/document/getelementsbytagname>`_,
* `document.getElementsByClassName <http://devdocs.io/dom/document/getelementsbyclassname>`_,
* `document.querySelector <http://devdocs.io/dom/document/queryselector>`_,
* `document.querySelectorAll <http://devdocs.io/dom/document/queryselectorall>`_

.. note::

   Il est plus efficace d'utiliser les méthodes ``getElement*``
   que d'utiliser ``querySelector*`` avec les sélecteurs correspondants
   (``tagname`` ou ``#identifier``).

Attributs et méthodes d'un élément
----------------------------------

``textContent``:
  permet de consulter *et modifier* le contenu textuel de l'élément

``style``:
  permet de consulter et modifier l'attribut ``style`` de l'élément,
  sous forme d'un objet ayant un attribut pour chaque propriété CSS.
  (:lat:`e.g.` ``e.style.fontSize`` pour la propriété ``font-size``)

  .. note::

     Pour faciliter l'utilisation en Javascript,
     la typographie des attributs de ``style`` n'est pas la même que celle des propriétés CSS correspondantes.
     Les tirets (``-``) sont remplacés par une mise en majuscule de la letter suivante
     (`CamelCase <https://en.wikipedia.org/wiki/CamelCase>`_).

.. nextslide::

``classList``:
  permet de consulter et modifier l'attribut ``class`` de l'élément,
  grâce aux méthodes suivantes :

  * ``add(cls)``: ajoute la classe `cls` a l'élément.

  * ``remove(cls)``: retire la classe `cls` a l'élément.

  * ``contains(cls)``: indique si l'élément possède actuellement la classe ``cls``.

  * ``toggle(cls)``: inverse l'état de la classe `cls` (présente/absente) sur l'élément.

  .. note::

     Comme en HTML+CSS,
     il est préférable de spécifier la mise en forme à l'aide de classes dans le CSS,
     et de modifier ces classes dans le code Javascript,
     plutôt que la spécifier directement dans le code Javascript à travers l'attribute ``style``.

.. nextslide::

Les éléments possèdent de nombreux autres attributs;
en particulier, chaque attribut HTML a une contrepartie en Javascript.

On peut notamment citer :

* ``href`` (pour les ``<a>``)
* ``src`` (pour les ``<img>``)
* ``value`` (pour les ``<input>``)
* ``disabled`` (pour tous les éléments de formulaire)
* ``checked`` (pour les cases à cocher)
* :lat:`etc`...

Expérimentez sur `cet exemple`__.

__ http://champin.net/enseignement/intro-js/_static/exemples/element_manipulation.html

Parcours du DOM
---------------

Récupérer des nœuds depuis un élément ``e``\  :

* `e.getElementsByTagName <http://devdocs.io/dom/element/getelementsbytagname>`_,
* `e.getElementsByClassName <http://devdocs.io/dom/element/getelementsbyclassname>`_,
* `e.querySelector <http://devdocs.io/dom/element/queryselector>`_,
* `e.querySelectorAll <http://devdocs.io/dom/element/queryselectorall>`_,
* `e.childNodes <http://devdocs.io/dom/node/childnodes>`_,
* `e.children <http://devdocs.io/dom/parentnode/children>`_,
* `e.parentNode <http://devdocs.io/dom/node/parentnode>`_,

Modification du DOM
-------------------

Création d'un nœud :

* `document.createElement <http://devdocs.io/dom/document/createelement>`_,
* `document.createTextNode <http://devdocs.io/dom/document/createtextnode>`_,
* `n.cloneNode <http://devdocs.io/dom/node/clonenode>`_

Une fois créé, le nœud est encore *hors* de l'arborescence du document
(et donc, non affiché).
Il est nécessaire de le rattacher à un nœud parent
par l'une des méthodes suivante :

* `n.insertBefore <http://devdocs.io/dom/node/insertbefore>`_,
* `n.replaceChild <http://devdocs.io/dom/node/replacechild>`_,
* `n.removeChild <http://devdocs.io/dom/node/removechild>`_,
* `n.appendChild <http://devdocs.io/dom/node/appendchild>`_





Intégration JS dans HTML
========================

Avertissement
-------------

Il existe de nombreuses méthodes.

Celle proposée ici vise à être simple et évolutive,
mais suppose un navigateur moderne.

Intégration d'un script à une page
----------------------------------

On include dans le HTML (dans le ``head`` ou le ``body``)
une balise ``script`` ayant la structure suivante :

.. code-block:: html

  <script src="url_du_script.js" defer></script>

Le script sera exécuté après le chargement complet du code HTML.

Programmation événementielle
----------------------------

* En programmation impérative classique,
  la fonction principale (``main``) décrit dans quel ordre
  les différentes fonctions du programme doivent s'exécuter.

* En programmation événementielle,
  on « abonne » chaque fonction à un (ou plusieurs) **événement(s)**.

* La fonction s'exécute
  lorsqu'un événement auquel elle est abonnée se produit.

* Les événement sont (souvent) liés aux interactions de l'utilisateur
  avec l'application.

Quelques événements utiles
--------------------------

* `click <http://devdocs.io/dom_events/click>`_
* `mouseover <http://devdocs.io/dom_events/mouseover>`_
* `keypressed <http://devdocs.io/dom_events/keypressed>`_
* `input <http://devdocs.io/dom_events/input>`_
* `change <http://devdocs.io/dom_events/change>`_
* `submit <http://devdocs.io/dom_events/submit>`_

Des listes plus exhaustives sont disponibles
`ici <http://www.w3schools.com/tags/ref_eventattributes.asp>`_
et `là <http://devdocs.io/dom_events/>`_.

.. note::

  Souvent, et pour des raisons historiques,
  les noms des événements sont préfixés par ``on``.
  Attention : le véritable nom de l'événement n'inclut *pas* ce préfixe.

Mise en œuvre
-------------

.. code:: js

  function incrementCounter() {
    let i = document.getElementsByTagName("input")[0];
    i.value = Number(i.value) + 1;
  }

  let b = document.querySelector("button");
  b.addEventListener('click', incrementCounter);

http://jsbin.com/mexuna/1/edit?html,js,output

.. note::

  * La méthode ``addEventListener`` associe un *comportement*
    (fonction) à un événement émis par un élément.

  * ⚠ Attention : le deuxième paramètre de ``addEventListener``
    est le **nom** d'une fonction, **sans** parenthèses
    (ce n'est pas un *appel* de la fonction).

Autre exemple
-------------

.. code:: js

  function incrementCounter() {
    let b = document.getElementsByTagName("body")[0];
    let p = document.createElement("p");
    p.textContent = "Vous avez cliqué";
    p.class.add("message");
    b.appendChild(p);
  }

  let b = document.querySelector("button");
  b.addEventListener('click', incrementCounter);
