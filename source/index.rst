.. Programmation Web pour la bio-informatique documentation master file, created by
   sphinx-quickstart on Sun Oct 22 19:19:18 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

============================================
 Programmation Web pour la Bio-Informatique
============================================

Enseignants
===========

* `Pierre-Antoine Champin <http://champin.net/>`_
* Guillaume Launay

Planning
========

.. list-table::
   :widths: 1 1 3
   :header-rows: 1

   * - Date
     - Heure
     - Séance
   * - 05/11/2018
     - 14h-16h
     - `tdp/td1`:doc:
   * - 07/11/2018
     - 14h-16h
     - Cours : `cours/cm1`:doc:
   * - 07/11/2018
     - 16h15-18h15
     - `tdp/tp1`:doc:
   * - 14/11/2018
     - 16h15-18h15
     - Cours : `cours/cm2`:doc:
   * - 19/11/2018
     - 14h-16h
     - `tdp/tp2`:doc:
   * - 21/11/2018
     - 14h-16h
     - `tdp/td2`:doc:
   * - 21/11/2018
     - 16h15-18h15
     - Cours : `cours/cm3`:doc:
   * - 28/11/2018
     - 14h-16h
     - `tdp/tp3`:doc:
   * - 28/11/2018
     - 16h15-18h15
     - Cours : `cours/cm4`:doc:
   * - 5/12/2018
     - 14h-16h
     - `tdp/tp4`:doc:
   * - 5/12/2018
     - 16h15-18h15
     - Cours : `cours/cm5`:doc:
   * - 12/12/2018
     - 14h-16h
     - `tdp/tp5`:doc:
   * - 12/12/2018
     - 16h15-18h15
     - Cours : `cours/cm6`:doc:
   * - 19/12/2018
     - 10h-12h
     - Fin du `tdp/tp5`:doc:
   * - 19/12/2018
     - 16h15-18h15
     - Examen

.. toctree::
   :maxdepth: 2
   :caption: Table des matières :

   cours
   tdp
   annexes


* :ref:`genindex`
