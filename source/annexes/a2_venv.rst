=======================
 Environnement virtuel
=======================

.. highlight:: bash

Un environnement virtuel est un répertoire contenant
une installation de Python autonome
(indépendante de celle du système d'exploitation).
Les environnements virtuels sont intéressants lorsqu'on développe,
notamment pour :

* avoir une version par défaut de Python différente de celle du système,
* installer des librairies dans une version différente de celle du système,
* installer des librairies sans les installer globalement sur le système,
* installer des librairies sans avoir les droits d'aministrateur.

Voici les rudiments à connaître pour utiliser un environnement virtuel.

* Pour créer un environnement virtuel, tapez la commande ::

    python -m venv env1

  où ``env1`` est le nom du répertoire qui sera créé.

* Pour *activer* l'environnement virtuel,
  c'est à dire configurer la session de l'utilisateur
  pour utiliser cette installation de Python plutôt que celle du système,
  tapez la commande ::

    source env1/bin/activate

  Générallement, l'invite de commande sera modifiée pour afficher,
  entre parenthèses, le nom de l'environnement virtuel (ici ``(env1)``).

* Pour instaler une librairie dans l'environnement virtuel,
  assurez-vous de bien l'avoir activé, puis tapez la commande ::

    pip install la_librairie

* Pour *désactiver* l'environnement virtuel,
  c'est à dire reconfigurer la session de l'utilisateur
  pour utiliser l'installation système de Python, tapez la commande ::

    deactivate
