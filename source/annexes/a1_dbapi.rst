=================================
 Connexion à une base de données
=================================

.. warning:: En travaux.

  En attendant, vous pouvez consulter :
  https://docs.python.org/3.6/library/sqlite3.html

.. todo::

   traduire le début de cette pages

   https://docs.python.org/3.6/library/sqlite3.html

   qui globalement est très bien faite et contient toute l'info que je veux mettre
   (y compris le lien vers XKCD 327!)
