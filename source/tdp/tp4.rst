=================================
 TP n°4 : Formulaire intelligent
=================================

Nous allons repartir du formulaire de création de gène,
développé au `TP n°2 <tp2>`:doc:,
pour lui ajouter de l'intelligence coté client.

Mise en jambes
==============

* Modifiez le formulaire HTML de sorte que
  le bouton de validation du formulaire soit désactivé
  ("grisé") par défaut.

* Ajoutez dans le formulaire HTML un lien vers un script
  (situé dans le répertoire ``static`` de votre projet).

* Créez ce script de sorte qu'il affiche simplement "Bonjour"
  dans la console.
  Rechargez le formulaire, et constatez que le script fonctionne.

* Modifiez votre script de sorte que "Bonjour" s'affiche maintenant
  chaque fois qu'une donnée est saisie dans le premier champ.

Passons aux choses sérieuses
============================

* Écrivez dans le script une fonction qui vérifie,
  pour tous les champs du formulaires pour lesquel cela est pertinent,
  si la donnée qu'il contient est valide
  d'après la sémantique de ce champ.
  Cette fonction doit retourner ``true`` si tous les champs sont valides,
  ``false`` sinon.

  NB: il pourra être pertinent de décomposer cette fontion en sous-fonctions.

* Écrivez maintenant une fonction qui appelle la fonction précédente,
  et active ou désactive le bouton de soumission en fonction du résultat.

* Abonnez cette dernière fonction à tous les événements pertinents,
  de sorte que l'état du bouton de soumission s'adapte
  au fur et à mesure de la saisie.

* Faites maintenant en sorte que,
  pour chaque champ contenant une valeur invalide,
  un message expliquant comment corriger l'erreur s'affiche à proximité de ce champ.

  NB: vous pouvez pour ce faire créer des éléments supplémentaires dans la page,
  ou simplement changer le contenu et la visibilité d'éléments présents dès le départ.



.. note::

  Ces vérifications coté client ne dispensent *aucunement*
  d'effectuer les vérifications coté serveur.
  En effet, le serveur n'a jamais la garantie
  que le code Javascript a été exécuté :
  les données POSTées peuvent l'être par d'autres biais,
  par exemple en utilisant les clients REST vus au
  `précédent cours <clients-rest>`:ref:.
