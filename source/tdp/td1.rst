=============================
 TD n°1 : Découverte de HTTP
=============================

.. role:: en

La console de développeur
=========================

Pour ce TD, vous allez vous familiariser avec la console de développeur,
disponible dans la plupart des navigateurs modernes.
Les instructions qui suivent concernent Firefox et Chrome ;
sur d'autres navigateurs, `YMMV (Your Mileage May Vary)`:abbr:.

.. list-table::
   :header-rows: 1

   * - Firefox
     - Chrome
   * -
       .. image:: ./console_firefox.*
           :alt: Console développeur sous Firefox
	   :width: 100%

     -
       .. image:: ./console_chrome.*
           :alt: Console développeur sous Chrome
	   :width: 100%

* Pour ouvrir/fermer la console, tapez ``F12``.
* La console peut apparaître à droite de la page, plutôt qu'en dessous.
* La console comporte plusieurs onglets.
  Celui qui nous intéresse dans cette séance est l'onglet ``Network``/``Réseau``.


Navigation
==========

* Ouvrez `cette page`__ **dans un nouvel onglet**.
* Dans cet onglet, ouvrez la console sur l'onglet navigation,
  puis cliquez sur le lien vers la page n°1.
* Vous constaterez qu'un certain nombre de lignes apparaissent dans la console.
  Chaque ligne représente un échange entre votre navigateur et un serveur.
* En passant le pointeur de la souris sur la colonne ``File``/``Fichier`` (sous Firefox)
  ou ``Name``/``Nom`` (sous Chrome), vous voyez apparaître l'URL complète.

  .. admonition:: Questions
     :class: note question

     * Faites une copie d'écran.
     * Combien d'échanges constatez vous lorsque vous cliquez sur le lien ?
     * Comment expliquez-vous ce nombre ?
       Vous pouvez vous aider en affichant le source HTML de la page
       (``CTRL+U``).

__ ../_static/td1/

* Cliquez maintenant sur le lien vers la page n°2.

  .. admonition:: Questions
     :class: note question

     * Faites une copie d'écran.
     * Combien d'échanges constatez vous cette fois ?
     * Quelles différences voyez-vous par rapport à la page précédente ?
       (indice : regardez notamment la colonne ``Status``/``État``)
     * Que signifie le code 404 ?

* Cliquez maintenant sur le lien vers la page n°3.

  .. admonition:: Questions
     :class: note question

     * Cela confirme-t-il votre réponse sur le code 404 ?

* Revenez en arrière pour ré-afficher la page n°2,
  et cliquez sur le lien vers la page n°1.

  .. admonition:: Questions
     :class: note question

     * Faites une copie d'écran.
     * Comparez le résultat avec votre première copie d'écran.
     * Recharchez la page avec ``CTRL+R``. Que constatez-vous ?
       Regardez notamment dans les colonnes ``Status``/``État``,
       et ``Transferred``/``Transfert`` (Firefox)
       ou ``Size``/``Taille`` (Chrome).
     * Recharchez la page avec ``CTRL+SHIFT+R``. Que constatez-vous ?

  .. hint::

     Le *cache* est une mémoire dans laquelle le navigateur stocke temporairement les informations récupérées sur le réseau, afin de gagner du temps lorsqu'il doit les réutiliser plusieurs fois de suite.

     Nous reparlerons plus en détail de cette notion dans une prochaine séance.


Requête et réponse
==================

* Chaque échange entre le navigateur et le serveur est constitué de deux messages :

  - une **requête** (`request`:en:), envoyée par le navigateur au serveur,
  - suivie d'une **réponse** (`response`:en:), envoyée par le serveur au navigateur.

* Recharchez la page avec ``CTRL+SHIFT+R``, puis cliquez sur la première ligne
  (``page1.html``).
  Un nouveau panneau apparaît, affichant le détail de la requête et de la réponse,
  et comportant plusieurs onglets.

* Cliquez sur l'onglet ``Response``/``Réponse``.
  Cet onglet contient le contenu de la réponse du serveur.

  .. admonition:: Questions
     :class: note question

     * Le contenu de l'onglet ``Réponse`` correspond-il à ce que vous attendiez ?
     * À votre avis, que contiendra cette onglet pour la ligne ``style.css`` ?
     * Cliquez maintenant sur la ligne ``style.css``.
       Le contenu de l'onglet ``Réponse`` correspond-il à ce que vous attendiez ?

* Cliquez sur l'onglet ``Headers``/``En-têtes``.
  Cet onglet les informations complémentaires includes
  dans les messages de requête et de réponse.

  .. admonition:: Questions
     :class: note question

     * Identifiez les trois parties de cet onglet, contenant
       1) les informations générales,
       2) les en-têtes de la réponse,
       3) les en-têtes de la requête.

     * Essayez de deviner, d'après leurs noms,
       lequel des en-têtes de requête (envoyés par le navigateur),
       indique au serveur quelle est la langue préférée de l'utilisateur.

     * Essayer de deviner, d'après leurs noms
       lequel des en-têtes de réponse (envoyés par le serveur),
       indique au navigateur la date de dernière modification du fichier envoyé.

     * Quelle est la méthode utilisée par cette requête
       (``Request method``/``Méthode de requête``) ?
       Voyez vous des requêtes utilisant d'autres méthodes ?

  .. hint::

     Au fur et à mesure de l'avancée du cours,
     nous découvrirons la signification précise d'un certain nombre d'en-têtes
     et de méthodes.

Formulaires
===========

* Depuis la page n°1, cliquez sur le lien vers le formulaire.

* Remplissez le avec des données quelconques, puis cliquez sur ``Envoyer``.

* Vous aboutissez sur une page récapitulant les données que vous avez saisies.

  .. admonition:: Questions
     :class: note question

     * Quelle est la méthode utilisée par cette requête
       (``Request method``/``Méthode de requête``) ?
       Est-ce conforme avec ce que la page affiche.

* Une requête POST se différencie d'une requête GET par le fait que le navigateur
  **envoie** des données au serveur.

* Ces données sont visibles dans le panneau de détail :

  - dans l'onglet ``Parameters``/``Paramètres`` sous Firefox,
  - en bas de l'onglet ``Headers``/``En-têtes`` sous Chrome.

  .. admonition:: Questions
     :class: note question

     * Affichez, dans l'onglet correspondant,
       le contenu de la requête.
       Est-ce conforme avec ce que la page affiche.

     * Revenez au formulaire et saisissez-y des informations différentes.
       Constatez dans la console que le contenu de la requête change en conséquence.

.. todo::

   2017-11-15 le TD ne leur a pas pris plus qu'une grosse heure

   * il faudrait notamment rajouter une page avec des liens internes,
     et leur faire constater qu'il n'y a pas d'échange avec le serveur dans ce cas.
