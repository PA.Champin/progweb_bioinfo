==========================================
 TP n°1 : Votre première application WSGI
==========================================

#. Créez deux fichiers ``hello.py`` et ``serveur.py`` contenant respectivement

   * l'exemple  `hello world <hello_world_wsgi>`:ref: du cours, et
   * le `script de lancement du serveur de développement <serveur_dev>`:ref:
     (en adaptant la première ligne),

   puis lancez ``serveur.py`` et connectez vous à http://localhost:12345/.

   Connectez-vous ensuite à

   + http://localhost:12345/toto
   + http://localhost:12345/tata
   + http://localhost:12345/titi?foo=bar

   Comment expliquez-vous ce comportement ?

   .. note::

    Le HTML retourné par le serveur n'est pas valide,
    mais nous exploitons le fait que les navigateurs sont assez robustes.
    Par la suite, nous améliorerons le serveur pour rendre le HTML valide.


#. Modifiez ``hello.py`` pour que

   * le message s'affiche uniquement sur l'URL http://localhost:12345/hello, et que
   * pour toute autre URL, le serveur réponde par un statut 404
     (avec un message d'erreur approprié).

#. Modifiez ``hello.py`` pour que, en plus du comportement actuel,

   * l'URL http://localhost:12345/goodbye, affiche le message "Goodbye world".

#. Refactorez ``hello.py`` en créant plusieurs fonctions WSGI :

   * ``hello`` affichant "Hello world",
   * ``goodbye`` affichant "Goodbye world",
   * ``erreur`` affichant le message d'erreur (avec le statut 404),
   * ``application`` déléguant le traitement à l'une des fonctions ci-dessus
     en fonction de l'URL.

#. Modifiez les fonctions ``hello`` et ``application``, de sorte que

   * toute URL de la forme http://localhost:12345/hello/X affiche "Hello X",
   * l'URL http://localhost:12345/hello redirige désormais vers http://localhost:12345/hello/world.

   Pour ce faire, la fonction ``application`` modifiera la valeur de ``PATH_INFO``
   transmise à la fonction ``hello``.

   .. note::

    Ce point et le suivant mettent en évidence une propriété importante de WSGI:
    la manière dont les fonctions WSGI peuvent se combiner pour construire des applications complexes.

#. Modifiez la fonction ``application`` pour qu'elle rajoute,
   avant et après le contenu retourné par les fonctions déléguées,
   le code nécessaire pour rendre le HTML valide,
   ainsi qu'un contenu commun à toute les pages
   (par exemple un titre "Ma première application WSGI").

   .. warning::

     Il sera éventuellement nécessaire de retirer les en-têtes ``content-length``,
     car les fonctions déléguées ne peuvent plus connaître à l'avance la taille totale du contenu retourné.

#. Modifiez la fonction ``goodbye`` de sorte que

   * toute URL de la forme http://localhost:12345/goodbye?name=X affiche "Goodbye X"

#. Modifiez la fonction ``erreur`` de sorte que le message d'erreur contienne
   des liens vers les pages existantes.

   En ce qui concerne la page ``goodbye``,
   vous incluerez un formulaire permettant de renseigner la valeur de ``name``.

#. Téléchargez le fichier `vers.txt <../_static/vers.txt>`_, et modifiez ``hello.py`` de sorte que:

   * l'URL http://localhost:12345/vers afiche une liste,
     donc chaque élément est une ligne du fichier ``vers.txt``.

#. Téléchargez le fichier `vers.sqlite <../_static/vers.sqlite>`_.
   Ce fichier est une base de données SQLite contenant une table ``vers``,
   composée de deux colonnes :

   * ``Id`` (entier)
   * ``Texte`` (chaîne de caractères)

   Modifiez ``hello.py`` de sorte que:

   * l'URL http://localhost:12345/vers2 afiche un tableau,
     donc chaque ligne de la table ``vers``.

  Vous trouverez des indications sur SQLite dans l'annexe
  `../annexes/a1_dbapi`:doc:.

.. JE VIRE LE CONSEIL CI-DESSOUS,
   parce qu'en fait, c'est compliqué de faire cela de manière robuste
   (notamment en multi-thread)

   NB: il convient de créer la connexion à la base de données *en dehors* des fonction WSGI,
   de sorte que la connexion ne se fasse qu'une seule fois au chargement du script.
