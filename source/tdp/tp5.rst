===============
 TP n°5 : AJAX
===============

.. role:: en

.. role:: lat

Nous allons continuer notre travail sur le formulaire intelligent du
`précédent TP<tp4>`:doc:,
en l'interfaçant avec l'API du `TP n°3 <tp3>`_.

Vérification des identifiants
=============================

Nous allons d'abord vérifier que l'identifiant saisi dans le formulaire
n'existe pas déjà dans la base.

* Abonnez une fonction à l'événement ``change`` sur le champs
  qui contient l'identifiant.
  Cette fonction ne fera rien si la valeur saisie est considérée comme invalide
  (par le code écrit au TP précédent).

* Si la valeur semble valide,
  la fonction doit déterminer l'URL du Gène correspondant dans l'API,
  et faire une requête AJAX avec la méthode HEAD sur cette URL.

* Pendant le chargement, à l'endroit du message d'erreur,
  il doit s'afficher un `spinner`:en:
  (par exemple : https://i.stack.imgur.com/qq8AE.gif).

* Lorsque la requête aboutit,
  le `spinner`:en: doit être supprimé,
  et un message d'erreur doit être affiché
  dans le cas ou un Gène avec l'identifiant proposé existe déjà.

Création `via`:lat: l'API
=========================

Nous allons maintenant modifier le formulaire
de sorte qu'il utilise l'API JSON pour créer un nouveau Gène.

* Supprimez les balise ``<form>`` et ``</form>`` du template HTML
  (tout en conservant leur contenu).
  Leur rôle est d'exécuter la requête POST,
  or c'est désormais le code Javascript qui va s'en charger.

* Écrivez une fonction ``form_to_json`` qui fabrique un objet JSON
  (conforme à `ce qu'attend l'API<api_post_gene>`:ref:)
  à partir des données saisies dans les champs ``input``.

* Abonnez à l'événement ``click`` sur le bouton du formulaire
  une fonction qui appelle ``form_to_json``
  et poste les données résultantes sur ``/api/Genes/``.

* Affichez dans la page Web un message indiquant le succès ou non de l'opération.

.. note::

  Une fois cette modification faite,
  nous pouvons retirer de notre programme Python
  le code qui traite les données provenant du formulaire,
  qui faisaient double-emploi avec l'API.

Création multiple
=================

Nous pouvons désormais profiter des fonctions avancées de l'API,
pour créer plusieurs Gènes en une seule opération.

* Modifiez le formulaire pour qu'il contiennent désormais deux boutons,
  "ajouter" et "envoyer", tous deux désactivés au départ.

* Le bouton "ajouter" est soumis aux contrôles du précédent TP
  (`i.e.`:lat: il n'est actif que lorsque les données sont valides),
  ainsi qu'au contrôle sur l'identifiant, décrit ci-dessus.

* Lorsqu'on clique sur le bouton "ajouter",
  l'objet JSON créé à partir des données du formulaire est ajouté à un tableau,
  stocké dans une variable partagée au niveau du script.

* Lorsque ce tableau partagé contient au moins un élément,
  le bouton "envoyer" s'active.

* Lorsqu'on clique sur le bouton "envoyer",
  le tableau partagé, contenant la description d'un ou plusieurs Gènes,
  est POSTé à l'API.
