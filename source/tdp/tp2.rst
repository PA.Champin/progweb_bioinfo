============================================
 TP n°2 : Une application plus intéressante
============================================

.. role:: en

Pré-requis
==========

* Créez un `environnement virtuel <../annexes/a2_venv>`:doc:.
* Installez-y ``flask`` avec la commande :

  .. code-block:: bash

    pip install flask

* Téléchargez la base de données
  `ensembl_hs63_simple.sqlite <../_static/ensembl_hs63_simple.sqlite>`_,
  dont le schéma relationnel vous est donné ci-dessous :

  .. figure:: ../_static/schema_relationnel.png
    :target:  ../_static/schema_relationnel.png

    Schéma de la base *ensembl_hs63_simple* (cliquez pour agrandir)

Cette base est un sous-ensemble de la base de données `Ensembl <https://www.ensembl.org/>`_.
Il s'agit d'une base de données d’annotations de génomes
contenant des informations sur les gènes, leurs transcrits,
ainsi que de nombreux autres renseignements comme la fonction des gènes
ou encore les comparaisons entre plusieurs génomes.

Votre travail
=============

Créez une application Flask répondant aux URLs suivantes :

``/``
  Une page de garde pointant notamment vers
  la liste des gènes.

``/Genes``
  Une page contenant les 1000 premièrs gènes de la table *Genes*,
  affichant pour chacun son identifiant (*Ensemble_Gene_ID*)
  et son *Associated_Gene_Name*.

  L'identifiant doit être un lien vers la fiche individuelle de ce gène
  (cf. ``/Genes/view/`` ci-dessous).

``/Genes/view/<id>``
  La fiche individuelle du gène ayant l'identifiant ``id``.

  Elle doit contenir toutes les informations données par la table *Genes*,
  ainsi que la liste de ses transcrits
  (accompagnés de leurs positions de départ et de fin).

``/Genes/del/<id>``
  Cette URL ne doit accepter *que les requêtes POST*.
  Dans ce cas, le gène ayant l'identifiant ``id``
  sera supprimé de la table *Genes*.

  Ajoutez à chaque ligne de la liste des gènes
  un bouton supprimant le gène concerné
  (il faut pour cela un formulaire dont l'action est l'URL appropriée,
  et contenant uniquement le bouton).

``/Genes/new``
  Un GET sur cette URL doit afficher un formulaire de saisie,
  permettant de décrire un nouveau gène.

  Un POST sur cette URL doit accepter les données du formulaire ci-dessus,
  vérifier leur validité,
  et créer l'entrée correspondante dans la table *Genes*,
  puis rediriger vers la listes des gènes.
  En cas de problème, un code d'erreur et un message appropriés
  doivent être retourné.

  Ajoutez au dessus de la liste des gènes un lien vers ce formulaire.


Utilisez les `templates`:en:
pour avoir une mise en page homogène sur toutes les pages du site,
avec une feuille de style définie comme une ressource statique.

Pour aller plus loin
====================

Vous pouvez augmenter votre site avec les URLs suivantes.

``/Genes/edit/<i>``
  un formulaire permettant de *modifier* un gène existant dans la base,
  accessible depuis la liste ou depuis la fiche individuelle.

``/Transcripts/...``
  une gestion des transcrits similaire à celle des gènes.
  Dans ce cas, il serait judicieux que les identifiants des transcrits
  apparaissant dans la fiche individuelle d'un gène soient des liens
  vers la fiche individuelle du transcrit correspondant.
  Inversement, la fiche individuelle d'un transcrit devrait lier
  vers la fiche du gène associé.
